// ==================THEORY=============================
/*
1. Опишіть своїми словами, що таке метод об'єкту.
Методом об'єкту є функція яка виступає у ролі властивості обєкту. Вона описує поведінку обєкту та виконує певні маніпуляції з іншими властивостями та значеннями об'єкту.

2. Який тип даних може мати значення властивості об'єкта?
Значення властивості може мати любий тип даних (рядок, число, масив, boolean, функція, об'єкт).

3. Об'єкт це посилальний тип даних. Що означає це поняття?
Посилальний тип даних означає те що, кожен об'єкт для зменшення обсягу ваги при багатоповторному використанні в коді, має свій персональний адрес(посилання). При створені обєкта створюється відповідно його індивідуальний адрес, і в подальшому викорстанні цей адрес слугує безпосердньо як шлях до цього об'єкту.
*/

// ==================PRACTICE==========================


// 1.

const product = {
    name: "Apple",
    price: 40000,
    discount: 6000,
    ultimatePrice() {
        return this.price - this.discount;
    },
}
console.log(product.ultimatePrice());



// 2.

const person = { 
    userName: prompt("Введіть будьласка ваше і'мя!"),
    age: parseInt(prompt("Введіть будьласка ваш вік!"), 10),
}


while(isNaN(person.age)){
    person.age = parseInt(prompt("Введіть будьласка ваш вік!"), 10);
}

function greeting (name, ages) {
    return alert(`Привіт, мене звати ${name} і мій вік ${ages}`);
}
console.log(person);
greeting(person.userName, person.age);



// 3.

const productTwo = {
    name: "Samsung",
    mark: "22 Ultra Pro",
    price: 30000,
    code: 442034,

    shipping : {
        adress: "Chornovola, 22",
        method: "car",
    },
}

console.log(productTwo);


const productThree = {};

for(key in productTwo){
    productThree[key] = productTwo[key];
}

productThree.name = "Apple";
console.log(productThree);

//АБО за допомогою подвійної конвертації JSON

// const productThree = JSON.parse(JSON.stringify(productTwo));

// productThree.name = "Apple";
// console.log(productThree);

